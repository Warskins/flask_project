"""
    Configuration object. Contains database URI, and run mode(debug, production or testing)
"""

class Configuration(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = ''
    SQLALCHEMY_TRACK_MODIFICATIONS = False

