from app import db


class Department(db.Model):
    """
        Model of Department
        id - unique number of department. Generates automatically.
        name - Full name of the department.

    """
    id = db.Column(db.INTEGER, primary_key=True)
    name = db.Column(db.String(50))
    children = db.relationship('Employee', back_populates="parent")

    def __init__(self, name):
        super(Department, self).__init__()
        self.name = name

    def __repr__(self):
        return f'Department id: {self.id}, name: {self.name}'
