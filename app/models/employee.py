from app import db


class Employee(db.Model):
    """
        Model of Employee
        id - unique number of employee. Generated automatically.
        first_name - First name of employee. Max length 20 symbols.
        last_name - Last name of employee. Max length 35 symbols.
        patronymic - Patronymic of employee. Max length 35 symbols.
        birthday - Date of birth. Format Y-mm-dd
        department_id - id of the department, employee belongs to. Foreign key.
        salary - Salary of the employee.
    """
    id = db.Column(db.INTEGER, primary_key=True)
    first_name = db.Column(db.String(20))
    last_name = db.Column(db.String(35))
    patronymic = db.Column(db.String(35))
    birthday = db.Column(db.Date)
    department_id = db.Column(db.INTEGER,
                              db.ForeignKey('department.id', ondelete='SET NULL'))
    salary = db.Column(db.INTEGER)
    parent = db.relationship('Department', back_populates="children")

    def __init__(self, *args, **kwargs):
        super(Employee, self).__init__(*args, **kwargs)

    def __repr__(self):
        return f'First name: {self.first_name}' \
               f' Last_name: {self.last_name}'

    def to_dict(self):
        return {"first_name": self.first_name,
                'id': self.id,
                'last_name': self.last_name,
                'patronymic': self.patronymic,
                'birthday': str(self.birthday),
                'department_id': self.department_id,
                'salary': self.salary}
