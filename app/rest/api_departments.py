from flask import jsonify, request
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import func

from models.department import Department
from models.employee import Employee
from app import app, db, logger


@app.route('/api/departments', methods=['POST'])
def create_department():
    """ Create new department

        Function operates with POST request from client.
        The request must contain a JSON object with the name of the new department.

        Returns status code 201, if request was correct and new entry has been added to database.

    """

    if request.json is None:
        return jsonify({'message:': 'Bad request'}), 400

    try:
        department = Department(**request.json)
        db.session.add(department)
        db.session.commit()
        logger.info('Created new department with name: {}'.format(department.name))
        return jsonify({"id": department.id, "name": department.name}), 201
    except (SQLAlchemyError, TypeError) as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message:': 'Bad request'}), 400


def average_salary(_id):
    res = db.session.query(func.avg(Employee.salary))\
        .filter(Employee.department_id == _id)\
        .scalar()
    return None if res is None else int(res)


@app.route('/api/departments', methods=['GET'])
def get_all_departments():
    """Get all departments

        Function processes GET request from client.
        If there are no problems with database connection,
        returns JSON object with all departments.
        In case of error, returns the message 'Database error' and status code 500.
        When there are no departments in database, it returns an empty string and status code 204.

    """
    try:
        departments = Department.query.all()
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message': 'Bad request'}), 400

    departments = [{"name": department.name,
                    "id": department.id,
                    "average_salary": average_salary(department.id)}
                   for department in departments]
    if len(departments) == 0:
        return '', 204
    return jsonify(departments), 200


@app.route('/api/departments/<int:_id>', methods=["GET"])
def get_department(_id):
    """Get information about a specific department

    Function processes GET request from the  client and require a unique id of the department.
    If there are no problems with database connection,
    returns JSON object with information about this department.
    In case of error, returns the message 'Database error' and status code 500.
    When there is no department in the database,
    it returns the message 'Not found' and status code 404.

    """
    try:
        department = Department.query.filter(Department.id == _id).first()
        if department is None:
            return jsonify({'name': 'None'}), 404

        employees = Employee.query.filter(Employee.department_id == _id).all()
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message': 'Bad request'}), 400

    return jsonify({"name": department.name,
                    "id": department.id,
                    "average_salary": average_salary(department.id),
                    'employees': [employee.to_dict() for employee in employees]}), 200


@app.route('/api/departments/<int:_id>', methods=['DELETE'])
def delete_department(_id):
    """Delete one existing employee from the database.

    The function operates with a DELETE request from a client.
    Argument '_id' must contain an id of the department to delete.
    If a department with this id doesn't exist, returns a 404 status code
    and JSON object with the message 'Not found.'
    After delete, all employees with this id automatically set their id to null.

    """
    if _id < 1:
        return jsonify({"message": "Bad request"}), 400
    try:
        result = Department.query.filter(Department.id == _id).delete()
        if not result:
            return jsonify({'message:': 'Not found'}), 404

        db.session().commit()
        logger.info('Deleted department with id {}'.format(_id))
        return jsonify(""), 204
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message:': 'Bad request'}), 400


@app.route('/api/departments/<int:_id>', methods=['PUT'])
def edit_department(_id):
    """Update exist department

    Use the variable _id to identify what department to update.
    Request object must contain a JSON object with new information.
    If a department with this id wasn't found, it returns the message 'Not found.'
    After successfully update, all employees depending on this department automatically update.

    """
    if _id < 1:
        return jsonify({"message": "Bad request"}), 400
    try:
        department = Department.query.filter(Department.id == _id)

        if department.count() == 0:
            return jsonify({"message": "Not found"}), 404

        department.update(request.json)
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({"message": "Bad request"}), 400

    db.session.commit()
    return jsonify(''), 204
