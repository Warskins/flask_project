 $(document).ready(function () {
            let url = location.href
            url = url.substring(url.lastIndexOf("/")+1)
            let page_id = parseInt(url)
            // load all employees and add them to table
             $.ajax({
                 type: "GET",
                 url: "/api/employees?department_id=" + page_id,
                 success: function (data) {
                     UpdateTable(data)
                 }
             });

            // filter between two dates
            $('#filter_form, #form_current_date').on('submit', function (e) {
                e.preventDefault()
                $.ajax({
                    type: "GET",
                    url: '/api/employees?' + $(this).serialize() + 'department_id='+ page_id,
                    success: function (data) {
                        UpdateTable(data)
                    }
                });
            })
        });

//update table after ajax get
        function UpdateTable(data) {
            let table = $("#employees_table tbody");
            table.html('')
            $.each(data, function (idx, elem) {
                table.append("<tr><td>" + elem.id + '</td><td>' + elem.department
                    + "</td>   <td>" + elem.first_name + "</td><td>" + elem.last_name
                    + "</td><td>" + elem.patronymic + "</td><td>" + elem.birthday
                    + "</td><td>" + elem.salary + `</td><td><a href='#' name='delete' id=${elem.id}>`
                    + 'Delete' + `</a></td><td><a name='update' href='#'  id=${elem.id}>` + 'Update' + "</a></td></tr>");
            });
        }




