        $(document).ready(function () {
            // load data to table after page load
            ajax_get_all('departments');

            // delete department
            $(document).on('click', 'a[name="delete"]', function () {
                ajax_delete('departments', $(this).attr('id'))
            })

            // add department
            $('#add_department_form').on('submit', function (e){
                e.preventDefault()
                submit_form_post($(this), 'departments',)
            })

            // update department
            $(document).on('click', 'a[name="update"]', function () {
                PopUpShowEdit()
                let id
                $.ajax({
                    url: '/api/departments/' + $(this).attr('id'),
                    type: 'GET',
                    success: function (data) {
                        id = data['id']
                        $(' input[name="name"]').val(data['name'])

                    }
                })
                // update employee witrhout submitting form
                $('#edit_department_form').on('submit', function (e) {
                    e.preventDefault()
                    submit_form_put($(this),'departments', id)
                })

            })
        });



        function UpdateTable(data) {
            let table = $("#departments_table tbody");
            table.html('')
            $.each(data, function (idx, elem) {
                table.append("<tr><td>" + elem.id + `</td><td><a href='${elem.id}'>` + elem.name
                    + "</a></td>   <td>" + elem.average_salary + `</td><td><a href='#' name='delete' id=${elem.id}>`
                    + 'Delete' + `</a></td><td><a name='update' href='#'  id=${elem.id}>` + 'Update' + "</a></td></tr>");
            });
        }

        //Show pop-up form
        function PopUpShow(number) {
            $("#popup"+number).show();
        }

