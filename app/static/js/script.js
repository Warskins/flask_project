//Load all employees or departments
function ajax_get_all(tableName) {
    $.ajax({
        type: "GET",
        url: "/api/" + tableName,
        success: function (data) {
            UpdateTable(data)
        }
    });
}

// get all date from form in JSON format
function JSON_from_form(form) {
    let json_obj = {}
    let data = form.serializeArray()
    $.each(data, function (idx, elem) {
        json_obj[elem['name']] = elem['value']
    })
    return json_obj
}

//Send post ajax  request without submitting form
// form = form object
// type - departments or employees
function submit_form_post(form, type) {
    let data =JSON.stringify(JSON_from_form(form))
    $.ajax({
        type: 'POST',
        url: '/api/' + type,
        contentType: "application/json",
        data: data,
        success: function (data) {
            alert('Success')
            location.reload()
        }
    })}


function submit_form_put(form,type,id){
    let data =JSON.stringify(JSON_from_form(form))
    $.ajax({
        type: 'PUT',
        url: '/api/' + type +'/'+id,
        contentType: "application/json",
        data: data,
        success: function (data) {
            alert('Success')
            location.reload()
        }
    })}

//Send delete ajax
//type - departments or employees
// id - id of department or employee to delete
function ajax_delete(type, id) {
    $.ajax({
        url: '/api/' + type +'/' + id,
        type: 'DELETE',
        contentType: "application/json",
        success: function () {
            alert('Deleted');
            location.reload()
        }
    })
}


//Hide pop-up  from
function PopUpHide(number) {
    $("#popup"+number).hide();
}

function PopUpShowEdit() {
    $('#popup2').show()
}

