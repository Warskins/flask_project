from tests.test_ import client


class TestWeb:

    def test_index(self, client):
        res = client.get('/')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'text/html; charset=utf-8'
        assert b'Main' in res.data
        assert b'Employee' in res.data
        assert b'Departments' in res.data

    def test_employees(self, client):
        res = client.get('/employees/')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'text/html; charset=utf-8'
        assert b'<table id="employees_table">' in res.data
        assert b'<div class="control_block">' in res.data

    def test_departments(self, client):
        res = client.get('/departments/')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'text/html; charset=utf-8'
        assert b'<table id="departments_table">' in res.data
        assert b'Add department' in res.data
