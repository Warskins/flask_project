from flask import render_template
from app import app
from models.department import Department


@app.route('/')
def index():
    """
        Index function. Return rendered main template(index.html)
    """
    return render_template('index.html')


@app.route('/employees/', methods=["POST", "GET"])
def show_employees():
    """
        Function returned rendered template(employees.html)
        Rendered code contains  all information about employees.
    """
    return render_template('employees.html')


@app.route('/departments/')
def show_departments():
    """
        Function returned rendered template(departments.html)
        Rendered code contains  all information about departments.
    """
    return render_template('departments.html')


@app.route('/departments/<int:_id>')
def show_department(_id):
    name = None
    dep = Department.query.filter(Department.id == _id).first()
    if dep is not None:
        name = dep.name
    return render_template('department.html', name=name)
