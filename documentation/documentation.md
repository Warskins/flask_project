# API

<h3><b>Departments</b></h3>

* /api/departments - POST <br>
    Accepts a json with one parameter: name. If department exists  confirmed message with status 401 is sent. Otherwise, a new department is created and response with status 201.
     
* /api/departments GET <br>
    Doesn't accept anything. Json object with all departments and  success status 200 returned. If not departments in database, status code 204 is returned.

* /api/departments/int:id GET <br>
    Doesn't accept anything. Json object with  one specific department and   success status 200 returned. If department don't exist, status code 204 is returned.

* /api/departments/int:id PUT <br>
    Accepts a json with one parameter: name. If department don't exists  confirmed message with status 404 is sent. Otherwise, an existed department is updated and response with status 201.

* /api/departments/int:id DELETE <br>
    Doesn't accept anything.  If department don't exist, status code 204 is returned. Otherwise department is deleted and status code 204 returned.
    

    


<h3><b>Employees</b></h3>

* /api/employees - POST <br>
    Accepts a json with six parameters: first_name, last_name, patronymic, birthday, salary, department_id. If employee exists  confirmed message with status 401 is sent. Otherwise, a new employee is created and response with status 201.
     
* /api/employees GET <br>
    Doesn't accept anything. Json object with all employees and  success status 200 returned. If not employee in database, status code 204 is returned.

* /api/employees/int:id GET <br>
    Doesn't accept anything. Json object with  one specific employee and   success status 200 returned. If department don't exist, status code 204 is returned.

* /api/employees/int:id PUT <br>
    Accepts a json with any of six parameter: first_name, last_name, patronymic, birthday, salary, department_id. If employee don't exists  confirmed message with status 404 is sent. Otherwise, an existed employee is updated and response with status 201.

* /api/employees/int:id DELETE <br>
    Doesn't accept anything.  If employee don't exist, status code 204 is returned. Otherwise department is deleted and status code 204 returned.